# Contributor: Mark Pashmfouroush <mark@markpash.me>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Mark Pashmfouroush <mark@markpash.me>
pkgname=coredns
pkgver=1.8.6
pkgrel=4
pkgdesc="fast and flexible DNS server"
url="https://github.com/coredns/coredns"
license="Apache-2.0"
arch="all"
options="net"
makedepends="go libcap unbound-dev"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/coredns/coredns/archive/v$pkgver.tar.gz
	coredns.confd
	coredns.initd
	coredns.logrotated
	ensure-cgo.patch
	plugin.cfg.enabled
	"

prepare() {
	default_prepare
	cp $srcdir/plugin.cfg.enabled $builddir/plugin.cfg
}

build() {
	make
}

check() {
	cd "$builddir"/request; go test ./...
	cd "$builddir"/core; go test ./...
	cd "$builddir"/coremain; go test ./...
	cd "$builddir"/plugin; go test ./...
}

package() {
	install -Dm755 coredns "$pkgdir"/usr/bin/coredns
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/bin/coredns

	install -Dm755 "$srcdir"/coredns.initd "$pkgdir"/etc/init.d/coredns
	install -Dm644 "$srcdir"/coredns.confd "$pkgdir"/etc/conf.d/coredns
	install -Dm 644 "$srcdir"/coredns.logrotated "$pkgdir"/etc/logrotate.d/coredns

	install -d "$pkgdir"/etc/coredns
	install -d "$pkgdir"/var/log/coredns
}

sha512sums="
589e05aaec71acd242aae69a68dacb2575fe9c436d7a318adbc963a3302a6e343a0c1a0966c9fb7819cec38bd91e3b4036e65b3080b5016514c24f26caad737e  coredns-1.8.6.tar.gz
90300a3035b00b58f362b2c12dfaeee21b889e10e90600523b5785c907dfbd7e515c3269dd29a9d4c758990e78ede8343edfac0bc022bd3a23652543ba2d42d3  coredns.confd
06d9fcf227e064f8ea21f4f003d33611aabf2d75b2e6e097dc10af1db42ea823d15c26649e52584da4fb4a85e87b3f27959a44ce873f176c2a082624f6845f38  coredns.initd
c697c08c3b1153ae224a1eeb3521f9ee594de4852bc6ef78d94dc7f6f680517659a653ab5c13ea03918deb79e6654ef11512ba4c90fe06ea4591fefbab876ece  coredns.logrotated
71d3a347cc8377c8db2ea3204ec4d5ad9b547c2330006f380e057f0945d4ef18cf2572b8104e2a7e1c30826438745e53874cedec3cbcc451cb2f0e21dc4b97b6  ensure-cgo.patch
92c586c07e695417a79536ea5f07bc412e373929916d82692c840a44454cb112604ab3be1cda04e290be9a4ed6cfef326ba76d38278f36490cb9944be265fe6d  plugin.cfg.enabled
"
