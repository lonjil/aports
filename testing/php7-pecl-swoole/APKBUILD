# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php7-pecl-swoole
_extname=swoole
pkgver=4.8.3
pkgrel=0
pkgdesc="Event-driven asynchronous and concurrent networking engine with high performance for PHP 7.4 - PECL"
url="https://pecl.php.net/package/swoole"
arch="all"
license="Apache-2.0"
depends="php7-curl php7-json php7-mysqlnd php7-openssl php7-sockets"
makedepends="php7-dev openssl1.1-compat-dev nghttp2-dev c-ares-dev curl-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir"/$_extname-$pkgver
subpackages="$pkgname-dev"
replaces="php7-pecl-openswoole"

case "$CARCH" in
	ppc64le|s390x) makedepends="$makedepends libucontext-dev" ;;
esac

build() {
	case "$CARCH" in
		ppc64le|s390x) export LDFLAGS="$LDFLAGS -lucontext" ;;
	esac
	phpize7
	./configure --prefix=/usr \
		--with-php-config=php-config7 \
		--enable-mysqlnd \
		--enable-openssl --with-openssl-dir=/usr \
		--enable-sockets \
		--enable-swoole-curl \
		--enable-swoole-json \
		--enable-cares \
		--enable-http2
	make
}

check() {
	# needs extra services to test all suite
	php7 -d extension=modules/swoole.so --ri swoole
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/php7/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/50_$_extname.ini
}

sha512sums="
09eec101de512c9964aba7903eeae217f1f85f5262131983b24f00992bfd7e1ca78c7d22d35489daf99b24c2254bc3ff0668158c0a428de0e2e9d366cf2904c7  php-pecl-swoole-4.8.3.tgz
"
