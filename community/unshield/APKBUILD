# Contributor: Clayton Craft <clayton@craftyguy.net>
# Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=unshield
pkgver=1.4.3_git20211021
_commit="c758ac017dda2f1735d385a360f82b673b898f4b"
pkgrel=0
pkgdesc="Extracts CAB files from InstallShield installers"
# Note: only tested on the following archs, may work on others too but not
# adding them until this has been confirmed
arch="x86_64 aarch64 armv7"
url="https://github.com/twogood/unshield"
license="MIT"
makedepends="cmake zlib-dev"
checkdepends="bash"
source="$pkgname-$pkgver.tar.gz::https://github.com/twogood/unshield/archive/$_commit.tar.gz"
subpackages="
	$pkgname-doc
	$pkgname-dev
	"
options="!check"
# Package includes unit tests but they fail to execute:
# https://github.com/twogood/unshield/issues/99
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build . \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=None
	make -C build
}

check() {
	UNSHIELD=$builddir/src/unshield bash run-tests.sh
}

package() {
	make -C build DESTDIR="$pkgdir" install
}
sha512sums="
d9304c63905fc3f9e9db59d76ee2701f998ae0a7c1e1da87107df45f0f5012683f068402ac496911ac95b631a5585afd7757443e4856b4effda21f725d915c00  unshield-1.4.3_git20211021.tar.gz
"
